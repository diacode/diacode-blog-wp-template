		</div>
  </div>
</div>

<section id="diacode_picks">
  <div class="container">
    <div class="subscription">
      <h2>Subscribe to Diacode Picks</h2>
      <p>Diacode Picks is a newsletter that we release periodically selecting the best resources we found on the interwebs</p> 
      <form action="http://diacode.us2.list-manage.com/subscribe/post?u=84b6b6aa12ade9f1f2b81d51a&amp;id=04aba6c42c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <input type="text" placeholder="Insert your email" name="EMAIL" />
        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-sm">
      </form>
    </div>

    <div class="releases">
        <!-- <div class="release">
          <div class="cal">
            <div class="month">JUN</div>
            <div class="day">23</div>
          </div>

          <div class="info">
            <h4><a href="#">Release #n</a></h4>
            <p><strong>15</strong> links</p>
          </div>
        </div> -->
    </div>
  </div>
</section>

<footer>
  <div class="container">

    <section>
      <div class="categories">
        <h3>CATEGORIES</h3>
        <?php craftsman_categories(); ?>
      </div>

      <div class="blog-subscription">
        <h3>BLOG SUBSCRIPTION</h3>

        <p>Subscribe to the blog to get instantly all our posts in your preferred RSS reader.</p>

        <p>
          <a href="http://feeds.feedburner.com/BlogDiacode" class="btn">
            Subscribe to RSS <i class="ion-social-rss"></i>
          </a>
        </p>

      </div>

      <div class="about">
        <h3>ABOUT DIACODE</h3>

        <p>Diacode is a full stack Ruby on Rails development shop. We craft apps for web and mobile with love and passion.</p>

        <h3>FOLLOW US AT</h3>

        <div class="social-links">
          <a href="http://twitter.com/diacode">
            <i class="ion-social-twitter"></i>
          </a>

          <a href="http://facebook.com/diacode">
            <i class="ion-social-facebook"></i>
          </a>

          <a href="http://github.com/diacode">
            <i class="ion-social-github"></i>
          </a>
        </div>
      </div>
    </section>

    <section class="eof">Diacode 2014. The content of this page is licensed under the MIT License.</section>
  </div>  
</footer>

<?php
	/*
	 * Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
