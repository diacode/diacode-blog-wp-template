<?php
/**
 * The loop that displays a single post
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	  <h2>
	    <a href="<?php the_permalink(); ?>" rel="bookmark">
	    	<?php the_title(); ?>
	    </a>
	  </h2>

	  <div class="meta">
	    <div class="published-at">
	      <i class="ion-ios7-calendar-outline"></i>
	      <?php twentyten_posted_on(); ?>
	    </div>
      <?php if ( count( get_the_category() ) ) : ?>
  	    <div class="categories">
  	      <i class="ion-folder"></i>
  	      <?php printf(__(get_the_category_list( ', ' ))) ?>
  	    </div>
  	  <?php endif; ?>
	  </div>

  	<div class="post-content">
			<?php the_content(); ?>
  	</div>

  	<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
			<div class="author">
        <img src="<?php echo get_avatar_url(get_the_author_meta('ID'), $size = 200) ?>" />

			  <div class="bio">
			    <h3 class="name"><?php printf( get_the_author() ); ?></h3>
			    
			    <div class="story">
			      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
			    </div>

			    <div class="social">
            <a href="<?= get_the_author_meta('twitter'); ?>">
			        <i class="ion-social-twitter"></i>
			        Twitter
			      </a>

			      <a href="<?= get_the_author_meta('github'); ?>">
			        <i class="ion-social-github"></i>
			        GitHub
			      </a>
			    </div>
			  </div>
			</div>
  	<?php endif; ?>
	</article>

	<?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>